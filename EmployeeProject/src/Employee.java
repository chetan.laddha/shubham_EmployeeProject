public class Employee implements Comparable<Employee>{

	private int id,sal;
	private String name;
	public static String sort_column = "id";

	public Employee(int id, int sal, String name) {
		super();
		this.id = id;
		this.sal = sal;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	private void setId(int id) {
		this.id = id;
	}
	public int getSal() {
		return sal;
	}
	private void setSal(int sal) {
		this.sal = sal;
	}
	public String getName() {
		return name;
	}
	private void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", sal=" + sal + ", name=" + name + "]";
	}
	@Override
	public int compareTo(Employee o) {
		//this logic can be reduced.
		if(sort_column.equalsIgnoreCase("id")){
			if(id == o.id)
				return 0;
			else {
				if(id < o.id)
					return -1;
				else
					return 1;
			}
		}
		else{
			if(sal == o.sal){
				return 0;
			}
			else{
				if(sal < o.sal){
					return -1;
				}
				else {
					return 1;
				}
			}
		}


	}

}
