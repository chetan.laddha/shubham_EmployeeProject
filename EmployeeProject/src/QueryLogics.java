import java.util.ArrayList;
import java.util.Collections;

public class QueryLogics {

	public ArrayList<Employee> wherequery(ArrayList<String> words) {

		ArrayList <Employee> sortedEmp = new ArrayList<Employee>();
		if(words.contains("where")){
			int where_index = words.indexOf("where");
			if(words.size() >= where_index +3){

				String where_column_cond = words.get(where_index+1);
				String operator_name = words.get(where_index+2);
				int condition_value = Integer.parseInt(words.get(where_index+3));

				ReturnColumnValue returnColumnValue = new ReturnColumnValue(); 
				for(int i=0;i<ReadFile.Employeedb.size();i++){
					Employee e =  ReadFile.Employeedb.get(i);

					int db_col_value =  returnColumnValue.returnColValue(where_column_cond, e);

					switch(operator_name){
					case ">" :  
						if(db_col_value>condition_value){
							sortedEmp.add(e);
						}
						break;
					case "<" :
						if(db_col_value>condition_value){
							sortedEmp.add(e);
						}
						break;
					case "=" :
						if(db_col_value==condition_value)	{
							sortedEmp.add(e);
						}
						break;
					default  : System.out.println("Use Appropriate operator");
					}
				}
			}
			else{
				System.out.println("Enter Query in Proper Format");
			}
		}
		else{
			sortedEmp = ReadFile.Employeedb;
		}

		return sortedEmp;
	}

	public ArrayList<Employee> sortbyquery(ArrayList<String> words,ArrayList<Employee> sortedEmp) {

		if(words.contains("sortby")){
			int sort_index = words.indexOf("sortby");
			if(sort_index+1 > words.size()){
				Employee.sort_column = words.get(sort_index+1);
				Collections.sort(sortedEmp);
			}
			else{
				System.out.println("Enter Query in Proper Format");
			}
		}
		return sortedEmp;
	}
}
