
public class ReturnColumnValue {
	
	int returnColValue(String Col_name, Employee e) {

		int emp_id = 0 ;
		int emp_salary = 0;
		switch(Col_name){
		case "id" : emp_id = e.getId(); 
					return emp_id; 
		case "salary" : emp_salary = e.getSal();
						return emp_salary; 
		default  : System.out.println("Use Appropriate Column name in where clause");
		}

		return 1;

	}
}
