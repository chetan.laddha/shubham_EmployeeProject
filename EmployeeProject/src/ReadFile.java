import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class ReadFile implements ReadInterface{
	public static ArrayList<String> tabel_columns = new ArrayList<String>();;
	public static ArrayList<Employee> Employeedb = new ArrayList<Employee>();;
	@SuppressWarnings("resource")
	public void read() {
		String path = "C://Users//shubhaml//Desktop//input.txt";
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(path));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String str;
		
		int count = 0;
		int sal;
		try {
			while(( str = br.readLine()) != null){
				if(str.equalsIgnoreCase("Employee")){	

					//Reading name code -start
					if(count == 0)
						tabel_columns.add(br.readLine().toLowerCase()); //name
					else{
						br.readLine();
					}
					String name = br.readLine(); //shubham
					//Reading name code -end

					//Reading Id code -start
					if(count==0){
						tabel_columns.add(br.readLine().toLowerCase()); //ID
					}
					else{
						br.readLine();
					}
					int id = Integer.parseInt(br.readLine()); //121

					//Reading Id code - End

					//Reading Salary -start
					if(count==0){
						tabel_columns.add(br.readLine().toLowerCase()); //SAL
					}
					else{
						br.readLine();
					}
					sal = Integer.parseInt(br.readLine());
					//Reading Salary -End
					count++;
					//Use Constructor
					Employee emp = new Employee(id, sal, name);
					Employeedb.add(emp);
				}
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
