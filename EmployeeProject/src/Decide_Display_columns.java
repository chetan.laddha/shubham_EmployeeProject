import java.util.ArrayList;


public class Decide_Display_columns {
	
	public ArrayList<String> display_columns(String query)
	{
		ArrayList<String> UserRequiredcolumns = new ArrayList<String>();
		//Decide columns to display -> code Start
		if(query.contains("*")){
			UserRequiredcolumns = ReadFile.tabel_columns;
		}
		else{
			if(query.contains("id")){
				UserRequiredcolumns.add("id");
			}
			if(query.contains("name")){
				UserRequiredcolumns.add("name");
			}
			if(query.contains("salary")){
				UserRequiredcolumns.add("salary");
			}
		}
		return UserRequiredcolumns;
		
	}
}
