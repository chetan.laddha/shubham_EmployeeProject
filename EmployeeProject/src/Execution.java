import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Execution {

	public static void main(String[] args) throws IOException {

		//Accept Query input
		System.out.println("Enter Query");
		Scanner sc = new Scanner(System.in);
		String query = sc.nextLine();
		query = query.toLowerCase();

		//Read input file
		ReadFile readFile =  new ReadFile();
		readFile.read();

		//Decide Columns to Display/operate on
		Decide_Display_columns decide_Display_columns =  new Decide_Display_columns();
		ArrayList<String> UserRequiredcolumns = decide_Display_columns.display_columns(query);
		
		Break_Query break_Query =  new Break_Query();
		ArrayList<String> words = break_Query.break_query_str(query);
		
		QueryLogics queryLogics =  new QueryLogics();
		ArrayList<Employee> sortedEmp = ReadFile.Employeedb;
		if(query.contains("where") || query.contains("sortby") ){
			if(query.contains("where")){
				sortedEmp = queryLogics.wherequery(words);
			}
			if(query.contains("sortby")){
				sortedEmp = queryLogics.sortbyquery(words, sortedEmp);
			}
			PrintData.printRecords(sortedEmp, UserRequiredcolumns);
		}
		else{
			PrintData.printRecords(ReadFile.Employeedb, UserRequiredcolumns);
		}
	}
}
