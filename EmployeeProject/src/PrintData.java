import java.util.ArrayList;

public class PrintData {

	public static void printRecords(ArrayList<Employee> employees, ArrayList<String> columns){
		System.out.println();
		System.out.println("No of Employees ::" + employees.size());
		for(int i=0; i<columns.size(); i++ ) {
			if(i == columns.size()-1) {
				System.out.println(columns.get(i) + "\t");
			}
			else {
				System.out.print(columns.get(i) + "\t");
			}
		}
		for(int j=0; j<employees.size();j++) {
			Employee emp = employees.get(j);
			if(j>0) {
				System.out.println();
			}
			for(int i=0;i<columns.size();i++) {	
				String str = columns.get(i);
				if(str.contains("id")) {
					System.out.print(emp.getId() +"\t");
				}
				if(str.contains("name")) {
					System.out.print(emp.getName()+"\t");
				}

				if(str.contains("salary")) {
					System.out.print(emp.getSal()+ "\t");
				}

			}
		}
	}
}
