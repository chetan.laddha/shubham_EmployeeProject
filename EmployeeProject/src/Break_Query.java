import java.util.ArrayList;

public class Break_Query {
	public ArrayList<String> break_query_str(String query){
		ArrayList<String> words = new ArrayList<String>();
		String[] result = query.split("\\s");

		for(int i=0;i<result.length;i++) {
			words.add(result[i]);
		}
		return words;
	}
}
